package plugin

import (
	"os"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
)

// Match is the stub for matching function that determines if the project
// matches a concrete analyzer plugin.
func Match(path string, info os.FileInfo) (bool, error) {
	if info.Name() == "xxx" { // TODO
		return true, nil
	}
	return false, nil
}

func init() {
	plugin.Register("xxx", Match) // TODO
}
